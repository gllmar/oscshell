#/bin/bash
LABEL=com.gllmar.oscshell
PLIST=~/Library/LaunchAgents/$LABEL.plist

if launchctl list | grep -q $LABEL; then
    echo "Service: $LABEL is running, unloading first"
    launchctl unload $PLIST
else
    echo "Service: $LABEL is not running"
fi

if [ -f "$PLIST" ]; then
    echo "removing $PLIST"
    rm $PLIST
    echo "Service: $LABEL removed"  
else 
    echo "$PLIST does not exist."
fi
