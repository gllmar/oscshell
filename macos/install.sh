#/bin/bash
SCRIPTPATH=$(dirname "$(readlink -f "$0")")
PARENTDIR=$(dirname "$SCRIPTPATH")
LABEL=com.gllmar.oscshell


python3 $SCRIPTPATH/update_plist.py $SCRIPTPATH/$LABEL.plist $PARENTDIR oscshell.py

cp $SCRIPTPATH/$LABEL.plist  ~/Library/LaunchAgents/


if launchctl list | grep -q $LABEL; then
    echo "Service $LABEL is running, unloading"
    launchctl unload ~/Library/LaunchAgents/$LABEL.plist
fi

launchctl load ~/Library/LaunchAgents/$LABEL.plist
launchctl start $LABEL
launchctl list | grep $LABEL
