import plistlib
import argparse
import os

def change_plist_path(plist_file, new_path, script_name):
    # Load the plist file
    with open(plist_file, 'rb') as f:
        plist_data = plistlib.load(f)
    
    # Update the path in the plist data
    script_path = os.path.join(new_path, script_name)
    plist_data['ProgramArguments'][1] = script_path

     # Update the paths in the plist data
    out_path = os.path.join(new_path, "output.log")
    plist_data['StandardErrorPath'] = out_path
    plist_data['StandardOutPath'] = out_path
    
    # Save the modified plist file
    with open(plist_file, 'wb') as f:
        plistlib.dump(plist_data, f)
    
    print("Path in plist file updated successfully.")

# Parse command line arguments
parser = argparse.ArgumentParser(description="Update path in plist file")
parser.add_argument("plist_file", type=str, help="Path to the plist file")
parser.add_argument("new_path", type=str, help="New path to set")
parser.add_argument("script_name", type=str, help="script name")
args = parser.parse_args()

# Call the function with the provided arguments
change_plist_path(args.plist_file, args.new_path, args.script_name)

