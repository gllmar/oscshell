import argparse
import subprocess
from threading import Thread
from pythonosc import dispatcher
from pythonosc import osc_server

# Define the OSC message handler function
def osc_handler(address, *args):
    # Extract the OSC string from the address
    osc_string = address
    print("Received OSC string:")

    # Check if the OSC string matches the desired value
    if osc_string == "/exec":
        # Concatenate all OSC message arguments into a single string
        command = " ".join(str(arg) for arg in args)
        # Optional: Print the received OSC message
        print(">>> Executing : ", command)

        # Launch the command as a subprocess
        subprocess.run(command, shell=True)


    else:
        # Optional: Print the received OSC message
        print("Received OSC string:", osc_string)
        # Optional: Print the arguments of the received OSC message
        print("OSC arguments:", args)

# Define the main function
def main():
    # Parse command line arguments for the port number
    parser = argparse.ArgumentParser(description="OSC Listener")
    parser.add_argument("--port", type=int, default=59999, help="The port to listen for OSC messages")
    args = parser.parse_args()

    # Create an OSC dispatcher and attach the handler function
    osc_dispatcher = dispatcher.Dispatcher()
    osc_dispatcher.set_default_handler(osc_handler)

    # Create an OSC server with the dispatcher and start listening on the specified port
    server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", args.port), osc_dispatcher)
    print("Listening on port", args.port)

    # Start the OSC server in a separate thread
    server_thread = Thread(target=server.serve_forever, daemon=True)
    server_thread.start()

    try:
        # Keep the main thread alive
        server_thread.join()
    except KeyboardInterrupt:
        # Terminate the OSC server
        server.shutdown()
        server_thread.join()
        print("OSC server stopped.")

if __name__ == "__main__":
    main()
